package model;

/**
 * Clasa care abstractizeaza tabelul product din baza de date
 */
public class Product {

    private int idProduct = 0;
    private String productName;
    private double price;
    private int quantity;

    /**
     * Constructor fara parametri care initializeaza un produs nou
     */
    public Product(){

    }

    /**
     *Constructor care creeaza un produs nou
     * @param p numele produsului
     * @param pr pretul per bucata
     * @param q cantitatea de pe stoc
     */
    public Product(String p, double pr, int q){
        this.productName = p;
        this.price = pr;
        this.quantity = q;
    }

    /**
     * Getter pentru ID
     * @return returneaza ID-ul produsului
     */
    public int getIdProduct() {
        return idProduct;
    }

    /**
     * Setter pentru ID
     * @param id noul ID atribuit produsului
     */
    public void setIdProduct(int id) {
        this.idProduct = id;
    }

    /**
     * Getter pentru nume produs
     * @return returneaza numele produsului
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Setter pentru numele produsului
     * @param productName noul nume atribuit produsului
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Getter pentru pret
     * @return returneaza pretul produsului
     */
    public double getPrice() {
        return price;
    }

    /**
     * Setter pentru pretul produsului
     * @param price noul pret atribuit produsului
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Getter pentru cantitate
     * @return returneaza cantitatea produsului
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Setter pentru cantitate
     * @param quantity noua cantitate atribuita produsului
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Transforma obiectul produs in String
     * @return returneaza reprezentarea sub forma de String
     */
    public String toString(){
        String s = " ";
        return this.getIdProduct() + s + this.getProductName() + s + this.getPrice() + s + this.getQuantity();
    }

}
