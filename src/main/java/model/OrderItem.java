package model;

/**
 * Clasa care abstractizeaza tabelul orderitem din baza de date
 */
public class OrderItem {

    private int idorderItem;
    private int idProduct;
    private int idOrder;
    private int quantity;

    /**
     * Constructor gol care initializeaza un obiect OrderItem nou
     */
    public OrderItem(){

    }

    /**
     * Constructor care initializeaza un OrderItem nou
     * @param idp ID-ul produsului care este comandat
     * @param ido ID-ul comenzii la care se face referinta
     * @param q cantitatea comandata
     */
    public OrderItem(int idp, int ido, int q){
        this.idProduct = idp;
        this.idOrder = ido;
        this.quantity = q;
    }

    /**
     * Getter pentru ID
     * @return returneaza ID-ul pentru OrderItem
     */
    public int getIdorderItem() {
        return idorderItem;
    }

    /**
     * Setter pentru ID
     * @param id noul ID pentru OrderItem
     */
    public void setIdorderItem(int id) {
        this.idorderItem = id;
    }

    /**
     * Getter pentru ID produs
     * @return returneaza ID-ul produsului din OrderItem
     */
    public int getIdProduct() {
        return idProduct;
    }

    /**
     * Setter pentru ID produs
     * @param idProduct noul ID al produsului comandat
     */
    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    /**
     * Getter pentru ID order
     * @return returneaza ID-ul comenzii la care se face referinta
     */
    public int getIdOrder() {
        return idOrder;
    }

    /**
     * Setter pentru ID order
     * @param idOrder noul ID al comnezii
     */
    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    /**
     * Getter pentru cantitate
     * @return returneaza cantitatea comandata
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Setter pentru cantitate
     * @param quantity noua cantitate comandata
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Transforma obiectul OrderItem in String
     * @return returneaza reprezentarea sub forma de String
     */
    public String toString(){
        String s = " ";

        return this.idorderItem + s + this.idProduct + s + this.idOrder + s + this.quantity;
    }
}
