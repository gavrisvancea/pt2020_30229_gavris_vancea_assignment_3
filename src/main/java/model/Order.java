package model;

/**
 * Clasa care abstractizeaza tabelul order din baza de date
 */
public class Order {

    private int idorder;
    private int idClient;

    /**
     * Constructor gol care initializeaza o noua comanda
     */
    public Order(){

    }

    /**
     *Constructor care initializeaza o comanda noua
     * @param idc ID-ul clientului la care se face referinta
     */
    public Order(int idc){
        this.idClient = idc;
    }

    /**
     * Getter pentru ID comanda
     * @return returneaza ID-ul comenzii
     */
    public int getIdorder() {
        return idorder;
    }

    /**
     * Setter pentru ID comanda
     * @param id noul ID al comenzii
     */
    public void setIdorder(int id) {
        this.idorder = id;
    }

    /**
     * Getter pentru ID Client
     * @return returneaza ID client care plaseaza comanda
     */
    public int getIdClient() {
        return idClient;
    }

    /**
     * Setter pentru ID Client
     * @param idClient noul ID al clientului atribuit comenzii
     */
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    /**
     * Transforma obiectul Order in String
     * @return returneaza reprezentarea sub forma de String
     */
    public String toString(){
        String s = " ";
        return idorder + s + idClient;
    }
}
