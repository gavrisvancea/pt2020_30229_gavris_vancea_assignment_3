package dao;

import connection.ConnectionFactory;
import model.Order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Clasa care mosteneste AbstractDAO si are ca parametru generic clasa Order care face
 * abstractizarea tabelului order din baza de date
 */
public class OrderDAO extends AbstractDAO<Order>{
    private static final String FIND_IDCLIENT = "SELECT * FROM gavris.order WHERE idClient = ?";
    private static final String DELETE = "DELETE FROM gavris.order WHERE idorder = ?";

    /**
     * se face cautarea dupa ID-ul clientului care a plasat comanda
     * @param id ID-ul clientului
     * @return returneaza comanda plasata de clientul specificat
     */
    public Order findByIdClient(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(FIND_IDCLIENT);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return this.createObjects(resultSet).get(0);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * sterge o comanda dupa un ID dat
     * @param id ID-ul dupa care se face stergerea comenzii
     */
    public void deleteOrder(int id){
        Connection connection = null;
        PreparedStatement statement = null;

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
}
