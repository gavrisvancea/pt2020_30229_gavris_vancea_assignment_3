package dao;

import connection.ConnectionFactory;
import model.Client;
import model.Product;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clasa care crealizeaza comenzile SQL comune pentru fiecare tabel
 * @param <T> parametrul care face referinta la numele tabelului
 */
public class AbstractDAO<T> {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> cls;

    /**
     * Constructor care initializeaza variabila
     * instanta si acceseaza Clasa parametrului generic
     */
    public AbstractDAO() {
        this.cls = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * se creeaza comanda generica ce trebuie executata in SQL
     * sub form a de String
     * @param field coloana din baza de date care conditioneaza cautarea
     * @return returneaza comanda sub forma de String
     */
    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM gavris.");
        sb.append(cls.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    /**
     * se face cautarea in baza de date dupa coloana ID
     * @param id criteriul de cautare
     * @return clasa trimisa ca parametru generic in urma executarii comenzii SQL
     * @throws NoSuchMethodException Nu s-a putut executa comanda
     */
    public T findById(int id) throws NoSuchMethodException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id" + cls.getSimpleName());
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, cls.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * creeaza comanda SQL generica pentru inserarea in baza de date
     * @return returneaza sub forma de String comanda din SQL
     */
    private String createInsert(){

        StringBuilder fields = new StringBuilder();
        StringBuilder vars = new StringBuilder();
        String sql = "INSERT INTO gavris." + cls.getSimpleName();

        for(Field f: cls.getDeclaredFields()){

            if(fields.length() > 1){
                fields.append(", ");
                vars.append(", ");
            }
            fields.append(f.getName());
            vars.append("?");
        }
        sql = sql + " " + "(" + fields + ") VALUES (" + vars + ")";
        return sql;
    }

    /**
     * inserarea unui obiect in baza de date
     * @param t Obiectul care trebuie inserat in baza de date
     * @return clasa trimisa ca parametru generic in urma executarii comenzii SQL
     */
    public T insert(T t) {

        Connection connection = null;
        PreparedStatement statement = null;
        String ins = createInsert();
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(ins);
            Field[] fields = cls.getDeclaredFields();

            for(int i = 0; i < fields.length; i++){
                Field f = fields[i];
                f.setAccessible(true);
                T value = (T) f.get(t);
                statement.setObject((i+1), value);
            }
            statement.executeUpdate();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    /**
     * creeaza comanda SQl generica pentru a face update
     * @return returneaza sub forma de String comanda de update din SQL
     */
    private String createUpdate(){
        StringBuilder set = new StringBuilder();
        String sql = "UPDATE gavris." + cls.getSimpleName() + " SET ";
        String whereClause = "";
        for(Field f: cls.getDeclaredFields()){
            String name = f.getName();
            String pair = name + " = ?";
            if(name.equals("id" + cls.getSimpleName())) {
                whereClause = " WHERE " + pair;
            }
            else{
                if(set.length() > 1){
                    set.append(", ");
                }
                set.append(pair);
            }
        }
        sql += set + " " + whereClause;
        return sql;
    }

    /**
     * se face update la un obiect din baza de date cu un obiect nou
     * @param t Obiectul care il va inlocui pe cel din baza de date gasit in urma cautarii dupa ID
     * @param id ID-ul(fiind unic) la care se face Update
     * @return clasa trimisa ca parametru generic in urma executarii comenzii SQL
     */
    public T update(T t, int id) {

        Connection connection = null;
        PreparedStatement statement = null;
        String sql = createUpdate();
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sql);
            Field[] fields = cls.getDeclaredFields();

            for(int i = 0; i < fields.length; i++){
                Field f = fields[i];
                f.setAccessible(true);
                T value = (T) f.get(t);
                if(f.getName().contains("id"))
                {
                    statement.setObject(fields.length, id);
                }
                else
                {
                    statement.setObject(i, value);
                }
            }
            statement.executeUpdate();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    /**
     * executa comanda SQl de cautare neconditionata a datelor
     * @return lista de date din tabelul corespunzator parametrului generic
     */
    public List<T> report(){

        //List<T> lst = new ArrayList<T>();

        String sql = "SELECT * FROM gavris." + cls.getSimpleName();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Transforma datele din baza de date in obiecte in functie de parametru generic
     * @param resultSet datele rezultate in urma executarii unei comenzi SQL din baza de date
     * @return lista de date create ca Obiecte din baza de date
     * @throws NoSuchMethodException Eroare in cazul in care nu exista o metoda
     */
    protected List<T> createObjects(ResultSet resultSet) throws NoSuchMethodException {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                T instance = cls.newInstance();
                for (Field field : cls.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), cls);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

}
