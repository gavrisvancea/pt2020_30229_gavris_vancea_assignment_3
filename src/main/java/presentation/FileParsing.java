package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.OrderItemBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.OrderItem;
import model.Product;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Clasa care se ocupa de traducerea fisierului de intrare in comenzi SQL executabile
 */
public class FileParsing {

    /**
     * Constructorul care realizeaza impartirea fisierului in comenzi SQL, apeleaza metodele cerute pentru fiecare
     * tabel din baza de date si genereaza fisiere PDF pentru afisarea datelor si facturi pentru comenzile plasate
     * @param inputFile numele fisierului din care se face citirea
     */
    public FileParsing(String inputFile){

        File fil = new File(inputFile);

        try{
            Scanner in = new Scanner(fil);

            while(in.hasNextLine()){
                String s = in.nextLine();
                if(s.contains("Insert")){
                    String[] str = s.split("\\: ");
                    String[] r = new String[10];
                    for(String i: str){
                        r = i.split("\\, ");
                    }
                    if(str[0].contains("client")){
                        ClientBLL client = new ClientBLL();
                        Client c = new Client(r[0], r[1]);
                        client.insertClient(c);
                    }
                    else if(str[0].contains("product")){
                        ProductBLL products = new ProductBLL();
                        Product p = new Product(r[0], Double.parseDouble(r[2]), Integer.parseInt(r[1]));
                        products.insertProduct(p);
                    }
                }
                else if(s.contains("Order")){
                    String[] str = s.split("\\: ");
                    String[] r = new String[10];
                    for(String i: str){
                        r = i.split("\\, ");
                    }
                    OrderBLL orders = new OrderBLL();
                    ClientBLL clients = new ClientBLL();
                    Order o = new Order(clients.findByName(r[0]).getIdClient());
                    orders.insert(o);

                    ProductBLL products = new ProductBLL();
                    OrderItemBLL orderitems = new OrderItemBLL();
                    OrderItem oi = new OrderItem(products.findByName(r[1]).getIdProduct(),orders.findById(clients.findByName(r[0]).getIdClient()).getIdorder() , Integer.parseInt(r[2]));
                    orderitems.insertOrderItem(oi);
                }
                else if(s.contains("Delete")){
                    String[] str = s.split("\\: ");
                    String[] r = new String[10];
                    for(String i: str){
                        r = i.split("\\, ");
                    }
                    if(str[0].contains("client")){
                        ClientBLL client = new ClientBLL();
                        client.deleteClient(client.findByName(r[0]).getIdClient());
                    }
                }
                else if(s.contains("Report")){
                    String str[] = s.split("\\ ");
                    PDFGenerator generator = new PDFGenerator();
                    generator.createTable(str[1]);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
