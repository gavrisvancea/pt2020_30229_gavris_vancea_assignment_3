package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.OrderItemBLL;
import bll.ProductBLL;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.stream.Stream;

import model.Client;
import model.Order;
import model.OrderItem;
import model.Product;

/**
 *  Clasa care creeaza fisirele PDF pentru afisarea datelor din baza de date si creeaza facturi pentru comenzile plasate
 */
public class PDFGenerator {

    private Document mydoc;

    /**
     * Constructor care initializeaza documentul PDF in care se va face scrierea
     */
    public PDFGenerator(){
        this.mydoc = new Document();
    }

    /**
     * metoda care creeaza header-ul tabelelor pentru afisarea datelor din baza de date
     * @param table tabelul in care se va adauga Header-ul
     * @param dbtable numele tabelului din baza de date pentru care se va face crearea tabelului
     */
    private void pdfHeader(PdfPTable table, String dbtable){
        if(dbtable.equals("client")){
            Stream.of("ID", "Name", "Address")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.WHITE);
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        }
        else if(dbtable.equals("product")){
            Stream.of("ID", "Product Name", "Price", "Quantity")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.WHITE);
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        }
        else if(dbtable.equals("order")){
            Stream.of("ID", "Client ID")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.WHITE);
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        }
        else if(dbtable.equals("orderitem")){
            Stream.of("ID", "Product ID", "Order ID", "Quantity")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.WHITE);
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        }
    }

    /**
     * metoda care adauga date in campurile unui tabel
     * @param table tabelul in care se vor adauga datele
     * @param dbtable numele tabelului din baza de date din care se vor aduga datele
     */
    private void addRows(PdfPTable table, String dbtable) {
        if(dbtable.equals("client")){
            ClientBLL clients = new ClientBLL();
            for(Client i: clients.reportClient()){
                table.addCell(Integer.toString(i.getIdClient()));
                table.addCell(i.getName());
                table.addCell(i.getAddress());
            }
        }
        else if(dbtable.equals("product")){
            ProductBLL products = new ProductBLL();
            for(Product i: products.reportProduct()){
                table.addCell(Integer.toString(i.getIdProduct()));
                table.addCell(i.getProductName());
                table.addCell(Double.toString(i.getPrice()));
                table.addCell(Integer.toString(i.getQuantity()));
            }
        }
        else if(dbtable.equals("order")){
            OrderBLL orders = new OrderBLL();
            for(Order i: orders.reportOrders()){
                table.addCell(Integer.toString(i.getIdorder()));
                table.addCell(Integer.toString(i.getIdClient()));
            }
        }
        else if(dbtable.equals(("orderItem"))){
            OrderItemBLL orderItems = new OrderItemBLL();
            for(OrderItem i: orderItems.reportOrderItems()){
                table.addCell(Integer.toString(i.getIdorderItem()));
                table.addCell(Integer.toString(i.getIdProduct()));
                table.addCell(Integer.toString(i.getIdOrder()));
                table.addCell(Integer.toString(i.getQuantity()));
            }
        }
    }

    /**
     * metoda care creeaza un fisier PDF in care se va aduaga tabelul de date din baza de date
     * @param tableName numele tabelului din baza de date pentru care va fi generat fisierul PDF
     */
    public void createTable(String tableName){
        try {
            PdfWriter.getInstance(mydoc, new FileOutputStream(tableName + ".pdf"));
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        mydoc.open();
        PdfPTable table = null;
        if(tableName.equals("client")){
            table = new PdfPTable(3);
        }
        else if(tableName.equals("product")){
            table = new PdfPTable(4);
        }
        else if(tableName.equals("order")){
            table = new PdfPTable((2));
        }
        else if(tableName.equals("orderitem")){
            table = new PdfPTable(4);
        }

        if(table != null){
            pdfHeader(table, tableName);
            addRows(table, tableName);
            table.setSpacingBefore(10);
        }
        try {
            Font font = FontFactory.getFont(FontFactory.COURIER, 20, BaseColor.BLACK);
            Chunk title = new Chunk(tableName, font);

            mydoc.add(title);
            mydoc.addTitle(tableName);
            mydoc.add(new Phrase("\n"));
            mydoc.add(table);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        mydoc.close();
    }

    /**
     * metoda care creeaza header-ul tabelului ce va contine produsele comnadate
     * pentru a se genera factura
     * @param table tabelul in care se va introduce header-ul
     */
    private void billHeaderTable(PdfPTable table){
        Stream.of("ID OrderItem", "Product Name", "Quantity", "Price/item", "Total Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.WHITE);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**
     * metoda care adauga date in tabelul de produse comandate pentru factura generata
     * @param table tabelul in care se adauga datele
     */
    private void addRowsBill(PdfPTable table){

        OrderItemBLL orderitems = new OrderItemBLL();
        ProductBLL products = new ProductBLL();
        for(OrderItem i: orderitems.reportOrderItems()){
            table.addCell(Integer.toString(i.getIdorderItem()));
            table.addCell(products.findById(i.getIdProduct()).getProductName());
            table.addCell(Integer.toString(i.getQuantity()));
            table.addCell(Double.toString(products.findById(i.getIdProduct()).getPrice()));
            double total = products.findById(i.getIdProduct()).getPrice() * i.getQuantity();
            table.addCell(Double.toString(total));
        }
    }

    /**
     * metoda care genereaza o factura pentru o comanda plasata
     * @param o comanda pentru care se genereaza factura
     */
    public void generateBill(Order o){
        ClientBLL clients = new ClientBLL();
        ProductBLL products = new ProductBLL();
        OrderItemBLL orderItems = new OrderItemBLL();
        int ok = 0;
        try {
            PdfWriter.getInstance(mydoc, new FileOutputStream( "bill" + o.getIdorder() + ".pdf"));
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mydoc.open();
        PdfPTable table = new PdfPTable(5);
        billHeaderTable(table);
        addRowsBill(table);
        try{
            Font fontTitle = FontFactory.getFont(FontFactory.COURIER_BOLDOBLIQUE, 22, BaseColor.BLACK);
            Font clientDetails = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
            Chunk title = new Chunk("Order number:" + o.getIdorder(), fontTitle);
            Chunk client = new Chunk("Customer: " + clients.findById(o.getIdClient()).getName(), clientDetails);
            Chunk clientAddress = new Chunk("Address: " + clients.findById(o.getIdClient()).getAddress(), clientDetails);
            Font cart = FontFactory.getFont(FontFactory.COURIER_BOLD, 14, BaseColor.BLACK);
            Chunk items = new Chunk("Shopping cart: ", cart);
            double total = 0;
            for(OrderItem i : orderItems.reportOrderItems()){
                total += products.findById(i.getIdProduct()).getPrice() * i.getQuantity();
            }
            Chunk totalPrice = new Chunk("Total: " + total, cart);

            mydoc.add(title);
            mydoc.add(new Phrase("\n"));
            mydoc.add(client);
            mydoc.add(new Phrase("\n"));
            mydoc.add(clientAddress);
            mydoc.add(new Phrase("\n"));
            mydoc.add(items);
            mydoc.add(new Phrase("\n"));
            mydoc.add(table);
            mydoc.add(new Phrase("\n"));
            mydoc.add(totalPrice);
            mydoc.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
}
