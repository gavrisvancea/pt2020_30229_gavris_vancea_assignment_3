package start;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.OrderItemBLL;
import bll.ProductBLL;
import dao.*;
import model.*;
import presentation.FileParsing;
import presentation.PDFGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Clasa care se ocupa cu executia programului
 */
public class MainClass {

    public static void main(String[] args) {

        ClientBLL clients = new ClientBLL();
        ProductBLL products = new ProductBLL();
        OrderBLL orders = new OrderBLL();
        OrderItemBLL orderitems = new OrderItemBLL();

        FileParsing f = new FileParsing(args[0]);

        for(Order i: orders.reportOrders().subList(0, orders.reportOrders().size() - 1)){
            PDFGenerator pdf = new PDFGenerator();
            pdf.generateBill(i);
        }
    }
}
