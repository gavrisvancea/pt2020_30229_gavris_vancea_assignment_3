package bll;

import dao.OrderDAO;
import dao.OrderItemDAO;
import dao.ProductDAO;
import model.Order;
import model.OrderItem;
import model.Product;

import java.util.List;

/**
 * Clasa care realizeaza logica de executare a comenzilor SQl pentru tabelul orderItem
 */
public class OrderItemBLL {

    private OrderItemDAO orderItemDB;

    /**
     * Constructor care initializeaza un obiect de tipul
     * OrderItemDAO pentru apelarea comenzilorSQL
     */
    public OrderItemBLL(){
        this.orderItemDB = new OrderItemDAO();
    }

    /**
     * insereaza un orderItem in baza de date;
     * daca s-a inserat cu succes se modifica valoarea cantitatii din depozit
     * @param orderItem orderItem-ul care se insereaza
     * @return returneaza 1 daca s-a inserat cu succes si 0 daca nu
     */
    public int insertOrderItem(OrderItem orderItem) {
        int ok = 0;
        OrderDAO ord = new OrderDAO();
        ProductDAO prod = new ProductDAO();
        List<Product> productList = prod.report();
        List<Order> orderList = ord.report();
        List<OrderItem> orderItemList = this.orderItemDB.report();
        try{
            if(orderItem.getIdOrder() == ord.findById(orderItem.getIdOrder()).getIdorder() && orderItem.getIdProduct() == prod.findById(orderItem.getIdProduct()).getIdProduct() && orderItem.getQuantity() <= prod.findById(orderItem.getIdProduct()).getQuantity()){
                ok = 1;
            }
            for(OrderItem o: orderItemList){
                if(o.getIdProduct() == orderItem.getIdProduct()){
                    ok = 0;
                }
            }
        }
        catch(Exception e){
            e.getMessage();
        }
        try {
            if (ok == 1) {
                this.orderItemDB.insert(orderItem);
                String sameName = prod.findById(orderItem.getIdProduct()).getProductName();
                double samePrice = prod.findById(orderItem.getIdProduct()).getPrice();
                int newQuantity = prod.findById(orderItem.getIdProduct()).getQuantity() - orderItem.getQuantity();
                Product newProd = new Product(sameName, samePrice, newQuantity);
                prod.update(newProd, orderItem.getIdProduct());
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return ok;
    }

    /**
     * cauta un orderItem dupa ID-ul dat
     * @param id ID-ul dupa care se face cautarea
     * @return returneaza orderItem-ul care are ID-ul egal cu cel dat
     */
    public OrderItem findById(int id){
        try {
            return this.orderItemDB.findById(id);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * accesarea listei de orderItems din tabelul orderItem
     * @return returneaza lista de orderItems din baza de date
     */
    public List<OrderItem> reportOrderItems(){
        return this.orderItemDB.report();
    }
}
